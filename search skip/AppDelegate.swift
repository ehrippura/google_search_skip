//
//  AppDelegate.swift
//  search skip
//
//  Created by Wayne Lin on 2019/3/4.
//  Copyright © 2019 Tzu-Yi Lin. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
}
